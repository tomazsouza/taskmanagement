package tom.dev.com.taskmanagement.constants

object TaskConstants {

    const val BASE_URL = "http://192.168.0.11:3000"
    const val EXTRA_REPLY = "tom.dev.com.taskmanagement.ui.form.SUCCESS_SAVE"
    const val ID_TASK_KEY = "tom.dev.com.taskmanagement.ui.form.ID_TASK"
    const val TASK_ACTIVITY_REQUEST_CODE = 1

}